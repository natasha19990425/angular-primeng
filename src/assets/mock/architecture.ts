import { TreeNode } from "primeng/api/treenode";

export class Architecture{
  architecture: TreeNode[] = [
    {
        label: 'AppComponent',
        expanded: true,
        children: [
            {
                label: 'Article',
                expanded: true,
                children: [
                    {
                        label: 'ArticleList',
                        expanded: true,
                        children: [
                          { label: 'ArticleHeader' },
                          { label: 'ArticleBody' },
                          { label: 'ArticleDialog' },
                          { label: 'ArticleSidebar' }
                        ]
                    }
                ]
            },
            {
                label: 'ArchitectureDiagram',
            }
        ]
    }
];
}
