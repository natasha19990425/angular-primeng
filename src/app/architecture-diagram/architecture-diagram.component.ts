import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
//primeng
import { OrganizationChartModule } from 'primeng/organizationchart';
import { Architecture } from 'src/assets/mock/architecture';

@Component({
  selector: 'his-architecture-diagram',
  standalone: true,
  imports: [
    CommonModule,
    //primeng
    OrganizationChartModule
  ],
  templateUrl: './architecture-diagram.component.html',
  styleUrls: ['./architecture-diagram.component.scss'],
  providers: [ Architecture ]
})
export class ArchitectureDiagramComponent {

  architectures: Architecture = inject(Architecture)
}
