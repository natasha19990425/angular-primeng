import { Injectable, inject } from '@angular/core';
import { Article } from './article.interface';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  constructor() {}

  http = inject(HttpClient);

  url:string = `http://localhost:3000/articles`;

  getArticles = async(): Promise<Article[]> => {

    const result$ =  this.http.get<Article[]>(`${this.url}`);
    return await lastValueFrom(result$);
   };

  removeArticle = async(id: number) => {

    const result$ =  this.http.delete(`${this.url}/${id}`);
    return await lastValueFrom(result$);
  };

  modifyArticle = async(article: Article) => {

    const result$ = this.http.put(`${this.url}/${article.id}`, article as Article);
    return await lastValueFrom(result$);
  }

}
