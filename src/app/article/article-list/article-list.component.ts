import { Article } from '../article.interface';
import { Component, OnInit, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleHeaderComponent } from './article-header/article-header.component';
import { ArticleBodyComponent } from './article-body/article-body.component';
import { ArticleDialogComponent } from './article-header/article-dialog/article-dialog.component';
import { ArticleSidebarComponent } from './article-sidebar/article-sidebar.component';
import { ArticleService } from '../article.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//primeng
import { PanelModule } from 'primeng/panel';

@Component({
  selector: 'his-article-list',
  standalone: true,
  imports: [
    CommonModule, ArticleHeaderComponent, ArticleBodyComponent, ArticleDialogComponent, ArticleSidebarComponent ,HttpClientModule, FormsModule, ReactiveFormsModule,
    //primeng
    PanelModule
  ],
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss'],
  providers: [ArticleService],
})
export class ArticleListComponent implements OnInit {

  articleService: ArticleService = inject(ArticleService);
  articles = signal([] as Article[]);

  async ngOnInit(): Promise<void> {

    this.articles.set(await this.articleService.getArticles());
  };

  async onRemoveArticle(article: Article) {

    try {
      await this.articleService.removeArticle(article.id);
      this.articles.mutate(x => {
        x.splice(x.findIndex(x => x.id === article.id),1);
      });
    } catch (e) {
      console.error(e);
    }
  };

  async onModifyArticle(article: Article) {
   try {
      await this.articleService.modifyArticle(article);
      this.articles.mutate(x => {
        x[x.findIndex(x => x.id === article.id)] = article;
      });
    } catch (e) {
      console.error(e);
    }
  };

}
