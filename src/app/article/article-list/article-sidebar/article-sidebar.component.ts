import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//primeng
import { CalendarModule } from 'primeng/calendar';
import { MenuModule } from 'primeng/menu';
import { CardModule } from 'primeng/card';
import { SidebarTitle } from 'src/assets/mock/sidebar';

@Component({
  selector: 'his-article-sidebar',
  standalone: true,
  imports: [
    CommonModule, FormsModule, ReactiveFormsModule,
    //primeng
    CalendarModule, MenuModule, CardModule
  ],
  templateUrl: './article-sidebar.component.html',
  styleUrls: ['./article-sidebar.component.scss'],
  providers: [ SidebarTitle ],
})
export class ArticleSidebarComponent {

  date!: Date;
  sidebarTitle: SidebarTitle = inject(SidebarTitle);

}
