import {Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArticleService } from '../../../article.service';
import { Article } from '../../../article.interface';
import * as moment from 'moment';
//primeng
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { DividerModule } from 'primeng/divider';

@Component({
  selector: 'his-article-dialog',
  standalone: true,
  imports: [ CommonModule, FormsModule, ReactiveFormsModule,
    //primeng
    InputTextModule, CalendarModule, DialogModule, InputTextareaModule, EditorModule, DividerModule,
  ],
  providers: [DatePipe],
  templateUrl: './article-dialog.component.html',
  styleUrls: ['./article-dialog.component.scss'],
})
export class ArticleDialogComponent implements OnInit {

  @Input() isDialogVisibled!:boolean;
  @Input() article!: Article;

  @Output() changes = new EventEmitter<Article>();
  @Output() dialogClosed = new EventEmitter();

  articleService: ArticleService = inject(ArticleService);
  originalArticle!: Article;
  datePipe: DatePipe = inject(DatePipe);
  date!: Date;


  ngOnInit(): void {

    this.originalArticle = this.article;
    this.date = new Date(this.article.date);
  }

  toggleDialog(){

    if(this.isDialogVisibled){
      this.isDialogVisibled = false;
    }
    else{
      this.isDialogVisibled = true;
    }
  }

  doModifyArticle() {

    this.article.date = moment(this.date).format('yyyy/MM/DD HH:mm');
    this.changes.emit(this.article);
    this.toggleDialog();
  }

  doCloseDialog() {

    this.dialogClosed.emit(this.originalArticle);
    this.isDialogVisibled = false;
  }
}
