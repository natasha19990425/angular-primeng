import { Article } from '../../article.interface';
import { Component, EventEmitter, Input, Output, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ArticleDialogComponent } from './article-dialog/article-dialog.component';
import { ArticleService } from '../../article.service';
//primeng
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'his-article-header',
  standalone: true,
  imports: [
    CommonModule, FormsModule, ArticleDialogComponent,
    //primeng
    ButtonModule, InputTextModule,
  ],
  templateUrl: './article-header.component.html',
  styleUrls: ['./article-header.component.scss']
})
export class ArticleHeaderComponent {

  @Input() item!: Article;
  @Output() delete = new EventEmitter<any>();
  @Output() titleChanged = new EventEmitter<any>();

  articleService: ArticleService = inject(ArticleService);

  isEdit = false;
  origItem!: Article;
  article = signal([] as Article[]);
  isDialogOpen = false;
  date!: Date;

  ngOnInit() {
    this.origItem = {...this.item}
  };

  async onModifyArticle(article: Article) {

console.log(article);
    this.titleChanged.emit(article);
  };

  doRemoveArticle() {

    this.delete.emit(this.item);
  };

  doDialogClosed() {
    this.item = {...this.origItem};
  }

}
