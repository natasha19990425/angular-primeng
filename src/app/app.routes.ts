import { Routes } from '@angular/router';

export const routes: Routes = [
  { path: '',redirectTo: 'ArticleList', pathMatch: 'full'},
  { path: 'ArticleList', loadComponent: () => import("./article/article-list/article-list.component").then(m => m.ArticleListComponent), title: "PrimengTest"},
  { path: 'ArchitectureDiagram', loadComponent: () => import("./architecture-diagram/architecture-diagram.component").then(m => m.ArchitectureDiagramComponent)}
];

